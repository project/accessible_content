<?php // $Id$

/**
*  @file Contains all the callbacks for service methods
*/

/**
*  The service callback for checking a string
*  @param $html The HTML string to cehck
*  @param $guideline_nid The node ID of the guideline to use
*  @return array An array of Quail results
*/
function accessible_content_service_check($html, $guideline_nid) {
  $results = accessible_content_check_string($html, $guideline_nid);
  return $results;
}