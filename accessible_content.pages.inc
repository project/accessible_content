<?php 

/**
*   @file
*   Pages for Accessible Content
*/

/**
*  Form for viewing the accessibility stats of a node. 
*
*/
function accessible_content_accessibility_tab_form() {
  drupal_add_css(drupal_get_path('module', 'accessible_content') .'/accessible_content.css');
  $node = menu_get_object();
  $form = array();
  
  $results = accessible_content_check_string($node->body, variable_get($node->type .'_accessibility_guideline_nid', 0));
  $form = array_merge($form, accessible_content_accessibility_report($results, 'body', 'Node Body'));
  $fields = variable_get('ac_field', array());

  foreach ($fields as $field => $check) {
    if ($check && property_exists($node, $field)) {
      $content_field = content_fields($field);
      if ($content_field['text_processing']) {
        foreach ($node->$field as $k => $field) {
          $number = (count($node->field) > 1) 
            ? ' #'. ($k+1)
            : '';
          $results = accessible_content_check_string($field['value'], variable_get($node->type .'_accessibility_guideline_nid', 0));
          $form = array_merge($form, 
                    accessible_content_accessibility_report($results, $k . $field, 
                    $content_field['widget']['label'] . $number,
                    array('field' => $content_field['field_name'], 'delta' => $k) ));  
        }
      }
    }
  }
  return $form;
}

/**
*  The demonstration or "highlight" tab
*
*/
function accessible_content_accessibility_demo_tab($nid) {
  drupal_set_title('Highlighted errors');
  global $base_url;
  accessible_content_include_library();
  $test_nid = arg(4);
  $node = menu_get_object();
  if (arg(5)) {
    //A field name was passed as argument 5, check that CCK field
    $field = arg(5);
    if (property_exists($node, $field)) {
      //If there is a argument 6, that is treated as the field number for multi-value fields
      $field = $node->$field;
      $text = (arg(6))
           ? $field[intval(arg(6))]['value']
           : $field[0]['value'];
  }
  //ERROR, the field doesn't exist in this node... do something!
  }
  else {
    $text = $node->body;
  }
  $quail = new quail($text, 'accessible_content', 'string', 'AccessibleContentHighlight');
  if ($test_nid) {
    $test = node_load(arg(4));
    $quail->runCheck(array('ac_module_testname' => $test->ac_function_name));
  }
  else {
    $guideline = node_load(variable_get($node->type .'_accessibility_guideline_nid', 0));
    $quail->runCheck(array('ac_module_guideline' => $guideline));
  }
  return $quail->getReport(array('image_url' => array(
          QUAIL_TEST_SEVERE => $base_url .'/'. 
            drupal_get_path('module', 'accessible_content') .'/images/test-demo-severe.png',
          QUAIL_TEST_MODERATE => $base_url .'/'. 
            drupal_get_path('module', 'accessible_content') .'/images/test-demo-moderate.png',
          QUAIL_TEST_SUGGESTION => $base_url .'/'. 
            drupal_get_path('module', 'accessible_content') .'/images/test-demo-suggestion.png')));
}